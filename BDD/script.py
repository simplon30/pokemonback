import sqlite3
import pandas as pd

# Liste de fichiers CSV à importer
csv_files = ['evolution.csv', 'pokedex.csv', 'pokemon.csv']  # Ajoutez vos fichiers ici

# Nom de la base de données SQLite
database_name = 'pokemon.db'

# Connexion à la base de données (ou création si elle n'existe pas)
conn = sqlite3.connect(database_name)
cursor = conn.cursor()

for csv_file in csv_files:
    # Chargement du fichier CSV dans un DataFrame pandas
    df = pd.read_csv(csv_file, sep=";")

    # Déterminer le nom de la table à partir du nom du fichier (sans extension)
    table_name = csv_file.split('.')[0]

    # Exporter le DataFrame vers une table SQLite
    df.to_sql(table_name, conn, if_exists='replace', index=False)

    print(f"Le fichier {csv_file} a été importé dans la table {table_name}.")

# Fermer la connexion à la base de données
conn.close()


# sudo mysql Pokedex -e "SELECT * FROM pokedex" | tr '\t' ';' > pokedex.csv