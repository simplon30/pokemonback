<?php

namespace App\Repository;


use App\Entities\EvolutionChain;
use App\Entities\Pokemon;
use App\Entities\PokemonData;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use PDO;
use App\Repository\PokedexRepository;

class PokemonRepository
{
    private PDO $connection;
    public function __construct(
        private HttpClientInterface $client,
    ) {
        $this->connection = Database::connect();
    }

    /**
     * @param string|int $name nom ou numéro du pokemon à donné pour la requête
     * @return array un pokemon
     */
    public function fetchPokemonbyName(string|int $name): array
    {
        //Pokemon
        // $content
        $response = $this->client->request(
            'GET',
            'https://pokeapi.co/api/v2/pokemon/' . $name
        );

        $content = $response->getContent();
        $content = $response->toArray();

        $keysToRemove = ['base_experience', 'forms', 'game_indices', 'held_items', 'is_default', 'order', 'past_types'];

        foreach ($keysToRemove as $key) {
            unset($content[$key]);
        }

        //Type
        $typeGroup = [];
        foreach ($content['types'] as &$key) {
            $url = $key['type']['url'];
            $response = $this->client->request(
                'GET',
                $url
            );
            $pkmnType = $response->toArray();
            $typeGroup[] = $pkmnType['names']['3']['name'];
        }
        $type = implode('|', $typeGroup);
        //Type

        //Liste attaques 2G
        $moves = $content['moves'];
        $gameToKeep = 'crystal';
        foreach ($moves as $move) {
            $versionGroupDetails = array_filter($move['version_group_details'], function ($entry) use ($gameToKeep) {
                return $entry['version_group']['name'] === $gameToKeep;
            });

            if (!empty($versionGroupDetails)) {
                $move['version_group_details'] = $versionGroupDetails;
                $filteredMoves[] = $move;
            }
        }
        $content['moves'] = $filteredMoves;
        //Liste attaques 2G

        $sprite = $content['sprites']['other']['official-artwork']['front_default'];
        $spriteShiny = $content['sprites']['other']['official-artwork']['front_shiny'];

        $height = $content['height'];
        //hectogrammes
        $weight = $content['weight'];

        //abilities
        $abilities = [];
        $abilitiesDesc = [];
        foreach ($content['abilities'] as $key) {
            $url = $key['ability']['url'];
            $response = $this->client->request(
                'GET',
                $url
            );
            $abilitiesContent = $response->toArray();
            $abilities[] = $abilitiesContent['names']['3']['name'];
            $filteredEntries = array_filter($abilitiesContent['flavor_text_entries'], function ($entry) {
                return $entry['version_group']['name'] === 'sword-shield' && $entry['language']['name'] === 'fr';
            });
            $filteredDescription = reset($filteredEntries)['flavor_text'];
            $abilitiesContent['flavor_text_entries'] = $filteredDescription;
            $flavorText = $abilitiesContent['flavor_text_entries'];
            $filteredDescription = str_replace('\u202f', '', $filteredDescription);
            $filteredDescription = implode('|', array_unique(explode('|', $filteredDescription)));
            $abilitiesDesc[] = str_replace("\n", ' ', $filteredDescription);
        }
        $abilities = implode('|', $abilities);
        $abilitiesDesc = implode('|', $abilitiesDesc);
        //abilities

        //STATS
        $hp = $content['stats']['0']['base_stat'];
        $attack = $content['stats']['1']['base_stat'];
        $defense = $content['stats']['2']['base_stat'];
        $specialAttack = $content['stats']['3']['base_stat'];
        $specialDefense = $content['stats']['4']['base_stat'];
        $speed = $content['stats']['5']['base_stat'];
        //STATS

        //Pokemon Species
        // $contenu
        $response = $this->client->request(
            'GET',
            'https://pokeapi.co/api/v2/pokemon-species/' . $name
        );

        $contenu = $response->toArray();

        $nom = $contenu['names']['4']['name'];
        $couleur = $contenu['color']['name'];

        //Description
        $filteredEntries = array_filter($contenu['flavor_text_entries'], function ($entry) {
            return $entry['version']['name'] === 'alpha-sapphire' && $entry['language']['name'] === 'fr';
        });
        $filteredDescription = reset($filteredEntries)['flavor_text'];
        $contenu['flavor_text_entries'] = $filteredDescription;
        $flavorText = $contenu['flavor_text_entries'];
        $filteredFlavorText = str_replace("\n", ' ', $flavorText);
        $description = $filteredFlavorText;
        //Description

        //Encounter
        $id = $content['id'];
        $response = $this->client->request(
            'GET',
            'https://pokeapi.co/api/v2/pokemon/' . $id . '/encounters'
        );
        $encounterContent = $response->toArray();
        if ($encounterContent) {
            //Seulement dans les jeux Crystal
            $filteredData = [];
            foreach ($encounterContent as $item) {
                $versionDetails = array_filter($item['version_details'], function ($detail) {
                    return $detail['version']['name'] === 'crystal';
                });
                if (!empty($versionDetails)) {
                    $item['version_details'] = array_values($versionDetails);
                    $filteredData[] = $item;
                }
            }
            //Location
            foreach ($filteredData as &$item) {
                $locationUrl = $item['location_area']['url'];
                $response = $this->client->request(
                    'GET',
                    $locationUrl
                );
                $encounterContent = $response->toArray();
                $item['location_data'] = $encounterContent;
            }
            //Garde que FR
            $isEncounterZone = false;
            foreach ($filteredData as $move) {
                $versionGroupDetails = array_filter($move['location_data']['names'], function ($entry) {
                    return $entry['language']['name'] === 'fr';
                });
                if (!empty($versionGroupDetails)) {
                    $move['names'] = $versionGroupDetails;
                    $filteredMovesEncounter[] = $move;
                    $isEncounterZone = true;
                }
            }
            if ($isEncounterZone) {
                $filteredData = $filteredMovesEncounter;
                // avoir que le texte dans "name"
                $result = [];
                foreach ($filteredData as $item) {
                    $names = $item['names'];
                    $name = $names['1']['name'];
                    $result[] = $name;
                }
                //Supprime les doublons
                // $result = array_unique($result);
                $encounter = implode('|', $result);
            } else {
                $encounter = "Zone Inconnue";
            }
        } else {
            $encounter = "Zone Inconnue";
        }
        //Encounter

        //Egg groupse
        foreach ($contenu['egg_groups'] as &$item) {
            $url = $item['url'];
            $response = $this->client->request(
                'GET',
                $url
            );
            $data = $response->toArray();
            $item['location_data'] = $data;
        }
        $eggGroups = [];
        foreach ($contenu['egg_groups'] as $group) {
            $name = $group['location_data']['names']['3']['name'];
            $eggGroups[] = $name;
        }
        $eggGroups = implode('|', $eggGroups);
        //fin Egg groups

        $genderRate = $contenu['gender_rate'];
        $captureRate = $contenu['capture_rate'];
        $genera = $contenu['genera']['3']['genus'];

        //shape
        $url = $contenu['shape']['url'];
        $response = $this->client->request(
            'GET',
            $url
        );
        $data = $response->toArray();
        $shape = $data['names']['0']['name'];
        //shape

        //evolutionID
        $evolutionUrl = $contenu['evolution_chain']['url'];
        $parts = explode('/', $evolutionUrl);
        $evolutionChainID = $parts[count($parts) - 2];
        //evolutionID


        $statement = $this->connection->prepare('INSERT INTO pokemon(
            nom,couleur,type,sprite,spriteShiny,description,height,weight,abilities,abilitiesDesc,encounters,Hp,Attack,Defense,specialAttack,specialDefense,speed,eggGroups,genderRate,captureRate,genera,shape,evolutionChainID
            )VALUES (
            :nom,:couleur,:type,:sprite,:spriteShiny,:description,:height,:weight,:abilities,:abilitiesDesc,:encounters,:Hp,:Attack,:Defense,:specialAttack,:specialdefense,:speed,:eggGroups,:genderRate,:captureRate,:genera,:shape,:evolutionChainID
            )');



        $statement->bindValue('nom', $nom);
        $statement->bindValue('couleur', $couleur);
        $statement->bindValue('type', $type);
        $statement->bindValue('sprite', $sprite);
        $statement->bindValue('spriteShiny', $spriteShiny);
        $statement->bindValue('spriteShiny', $spriteShiny);
        $statement->bindValue('description', $description);
        $statement->bindValue('height', $height);
        $statement->bindValue('weight', $weight);
        $statement->bindValue('abilities', $abilities);
        $statement->bindValue('abilitiesDesc', $abilitiesDesc);
        $statement->bindValue('encounters', $encounter);
        $statement->bindValue('Hp', $hp);
        $statement->bindValue('Attack', $attack);
        $statement->bindValue('Defense', $defense);
        $statement->bindValue('specialAttack', $specialAttack);
        $statement->bindValue('specialdefense', $specialDefense);
        $statement->bindValue('speed', $speed);
        $statement->bindValue('eggGroups', $eggGroups);
        $statement->bindValue('genderRate', $genderRate);
        $statement->bindValue('captureRate', $captureRate);
        $statement->bindValue('genera', $genera);
        $statement->bindValue('shape', $shape);
        $statement->bindValue('evolutionChainID', $evolutionChainID);
        $statement->execute();

        return $content;
    }

    /**
     * @return array tous les noms des pokemons en anglais
     */
    public function fetchAllPokemonbyName()
    {
        $httpClient = HttpClient::create();
        $pokedexRepo = new PokedexRepository($httpClient);
        $pokemonNames = $pokedexRepo->fetchPokemonName();
        $result = [];
        foreach ($pokemonNames as $key) {
            $result[] = $this->fetchPokemonbyName($key);
        }
        return $result;
    }

    /**
     * @return [evolutionChainID] toutes les ID des evolutions pour chaques Pokemon sans les doublons
     */
    public function getAllPokemonidEvolutionChain()
    {
        $query = $this->connection->prepare('SELECT DISTINCT evolutionChainID FROM pokemon ');
        $query->execute();
        return $query;
    }

    public function findAll(): array
    {
        $list = [];
        $query = $this->connection->prepare('SELECT * FROM pokemon');
        $query->execute();
        foreach ($query->fetchAll() as $line) {
            $pokemon = new Pokemon(
                $line['id'],
                $line['nom'],
                $line['couleur'],
                $line['type'],
                $line['sprite'],
                $line['spriteShiny'],
                $line['description'],
                $line['height'],
                $line['weight'],
                $line['abilities'],
                $line['abilitiesDesc'],
                $line['encounters'],
                $line['Hp'],
                $line['Attack'],
                $line['Defense'],
                $line['specialAttack'],
                $line['specialDefense'],
                $line['speed'],
                $line['eggGroups'],
                $line['genderRate'],
                $line['captureRate'],
                $line['genera'],
                $line['shape'],
                $line['evolutionChainID']
            );

            $list[] = $pokemon;
        }

        return $list;
    }

    public function findById(int $id)
    {
        $query = $this->connection->prepare('SELECT * FROM pokemon WHERE id = :id');
        $query->bindParam(':id', $id, PDO::PARAM_INT);
        $query->execute();
        $line = $query->fetch();

        if ($line === false) {
            return null;
        }

        $pokemon = new Pokemon(
            $line['id'],
            $line['nom'],
            $line['couleur'],
            $line['type'],
            $line['sprite'],
            $line['spriteShiny'],
            $line['description'],
            $line['height'],
            $line['weight'],
            $line['abilities'],
            $line['abilitiesDesc'],
            $line['encounters'],
            $line['Hp'],
            $line['Attack'],
            $line['Defense'],
            $line['specialAttack'],
            $line['specialDefense'],
            $line['speed'],
            $line['eggGroups'],
            $line['genderRate'],
            $line['captureRate'],
            $line['genera'],
            $line['shape'],
            $line['evolutionChainID']
        );
        $pokemon->setAbilitiesDesc(preg_replace('/\x{202F}/u', ' ', $pokemon->getAbilitiesDesc()));

        $requete = $this->connection->prepare('SELECT * FROM evolution WHERE idEvolutionChain = :requete');

        $evolutionChainID = $pokemon->getEvolutionChainID();

        $requete->bindParam('requete', $evolutionChainID, PDO::PARAM_INT);
        $requete->execute();
        $lines = $requete->fetchAll(PDO::FETCH_ASSOC); // Utilisez PDO::FETCH_ASSOC pour obtenir un array associatif

        if (!empty($lines)) {

            $evolutionChains = [];
            foreach ($lines as $line) {
                $evolutionChainID = $line['idEvolutionChain'];
                if (!isset($evolutionChains[$evolutionChainID])) {
                    $evolutionChains[$evolutionChainID] = [];
                }
                $evolutionChains[$evolutionChainID][] = new EvolutionChain(
                    $line['id'],
                    $line['idEvolutionChain'],
                    $line['evolveFromName'],
                    $line['evolveFromImg'],
                    $line['evolveToName'],
                    $line['evolveToImage'],
                    $line['evolveToLvl'],
                    $line['evolveToCondition'],
                    $line['evolveToBonheur'],
                    $line['evolveToTimeOfDay']
                );
            }

            $pokemonData = new PokemonData($pokemon, $evolutionChains[$pokemon->getEvolutionChainID()]);
            return $pokemonData;

        } else {
            $evolutionChains = [];
            $pokemonData = new PokemonData($pokemon, $evolutionChains);
            return $pokemonData;
            // return $pokemon;
        }
    }

    public function findByName(string $nom)
    {
        $query = $this->connection->prepare('SELECT * FROM pokemon WHERE nom = :nom');
        $query->bindValue(':nom', $nom);
        $query->execute();
        $line = $query->fetch();

        if ($line === false) {
            return null;
        }

        $pokemon = new Pokemon(
            $line['id'],
            $line['nom'],
            $line['couleur'],
            $line['type'],
            $line['sprite'],
            $line['spriteShiny'],
            $line['description'],
            $line['height'],
            $line['weight'],
            $line['abilities'],
            $line['abilitiesDesc'],
            $line['encounters'],
            $line['Hp'],
            $line['Attack'],
            $line['Defense'],
            $line['specialAttack'],
            $line['specialDefense'],
            $line['speed'],
            $line['eggGroups'],
            $line['genderRate'],
            $line['captureRate'],
            $line['genera'],
            $line['shape'],
            $line['evolutionChainID']
        );
        $pokemon->setAbilitiesDesc(preg_replace('/\x{202F}/u', ' ', $pokemon->getAbilitiesDesc()));

        $requete = $this->connection->prepare('SELECT * FROM evolution WHERE idEvolutionChain = :requete');

        $evolutionChainID = $pokemon->getEvolutionChainID();

        $requete->bindParam('requete', $evolutionChainID, PDO::PARAM_INT);
        $requete->execute();
        $lines = $requete->fetchAll(PDO::FETCH_ASSOC); // Utilisez PDO::FETCH_ASSOC pour obtenir un array associatif

        if (!empty($lines)) {

            $evolutionChains = [];
            foreach ($lines as $line) {
                $evolutionChainID = $line['idEvolutionChain'];
                if (!isset($evolutionChains[$evolutionChainID])) {
                    $evolutionChains[$evolutionChainID] = [];
                }
                $evolutionChains[$evolutionChainID][] = new EvolutionChain(
                    $line['id'],
                    $line['idEvolutionChain'],
                    $line['evolveFromName'],
                    $line['evolveFromImg'],
                    $line['evolveToName'],
                    $line['evolveToImage'],
                    $line['evolveToLvl'],
                    $line['evolveToCondition'],
                    $line['evolveToBonheur'],
                    $line['evolveToTimeOfDay']
                );
            }

            $pokemonData = new PokemonData($pokemon, $evolutionChains[$pokemon->getEvolutionChainID()]);
            return $pokemonData;

        } else {
            $evolutionChains = [];
            $pokemonData = new PokemonData($pokemon, $evolutionChains);
            return $pokemonData;
            // return $pokemon;
        }
    }







    
    public function delete(Pokemon $pokemon){
        $statement = $this->connection->prepare('DELETE FROM pokemon WHERE id = :id');
        $statement->bindValue('id', $pokemon->getId());
        $statement->execute();
    }

    public function update(Pokemon $pokemon) {
        $statement = $this->connection->prepare('UPDATE pokemon SET nom = :nom WHERE id = :id');
        $statement->bindValue('nom', $pokemon->getNom());
        $statement->bindValue('id', $pokemon->getId(), PDO::PARAM_INT);
        
        $statement->execute();
    }
}