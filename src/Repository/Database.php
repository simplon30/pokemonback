<?php

namespace App\Repository;
use PDO;
use PDOException;

class Database
{
    public static function connect()
    {
        // Chemin vers le fichier de base de données SQLite
        $db_file = __DIR__ . '/pokemon.db';

        // Connexion à SQLite avec PDO
        try {
            $pdo = new PDO("sqlite:$db_file");
            // Définir le mode d'erreur PDO sur Exception
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $pdo;
        } catch (PDOException $e) {
            // Gérer les erreurs de connexion ici
            die("Erreur de connexion à la base de données : " . $e->getMessage());
        }
    }
}