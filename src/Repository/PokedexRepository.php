<?php

namespace App\Repository;

use App\Entities\Pokedex;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use PDO;

class PokedexRepository
{
    private PDO $connection;

    public function __construct(private HttpClientInterface $client)
    {
        $this->connection = Database::connect();
    }

    /**
     * Cette fonction permet de fetch l'API, trier les données et ensuite de les insérer dans ma BDD
     */
    public function fetchPokedexData(): array
    {
        $response = $this->client->request(
            'GET',
            'https://pokeapi.co/api/v2/pokedex/3'
        );

        $content = $response->getContent();
        $content = $response->toArray();

        $keysToRemove = ['descriptions', 'is_main_series', 'name', 'names', 'region', 'version_groups'];

        foreach ($keysToRemove as $key) {
            unset($content[$key]);
        }

        $statement = $this->connection->prepare('INSERT INTO pokedex(nom,couleur,type,sprite,spriteShiny) 
        VALUES ( :nom , :couleur , :type , :sprite , :spriteShiny)');

        //Remplacer les noms en Francais
        //Route = pokemonSpecies + idPokemon
        foreach ($content['pokemon_entries'] as &$group) {
            $url = $group['pokemon_species']['url'];

            $response = $this->client->request(
                'GET',
                $url
            );

            $data = $response->getContent();
            $data = $response->toArray();

            //Route = Pokemon + pokemonName
            //Image
            $name = $group['pokemon_species']['name'];
            $url = "https://pokeapi.co/api/v2/pokemon/" . $name;

            $response = $this->client->request(
                'GET',
                $url
            );

            $contenu = $response->getContent();
            $contenu = $response->toArray();

            $sprite = $contenu['sprites']['other']['official-artwork']['front_default'];
            $spriteShiny = $contenu['sprites']['other']['official-artwork']['front_shiny'];
            //fin Image

            //Type
            $typeGroup = [];
            foreach ($contenu['types'] as &$key) {
                $url = $key['type']['url'];

                $response = $this->client->request(
                    'GET',
                    $url
                );

                $pkmnType = $response->getContent();
                $pkmnType = $response->toArray();

                $typeGroup[] = $pkmnType['names']['3']['name'];
            }
            $type = implode(',', $typeGroup);
            //fin Type

            //Nom
            $group['pokemon_species']['name'] = $data['names']['4']['name'];
            $nom = $group['pokemon_species']['name'];

            //Couleur
            $group['color']['name'] = $data['color']['name'];
            $couleur = $group['color']['name'];

            $statement->bindValue('sprite', $sprite);
            $statement->bindValue('spriteShiny', $spriteShiny);
            $statement->bindValue('nom', $nom);
            $statement->bindValue('couleur', $couleur);
            $statement->bindValue('type', $type);
            $statement->execute();
        }

        return $content;
    }

    /**
     * Cette fonction fait un GET vers pokeapi/pokedex pour récupérer tous les noms des Pokémon du Pokedex de la 2e génération.
     * 
     * @return array Tout les noms des Pokemon
     */
    public function fetchPokemonName(): array{
        $response = $this->client->request(
            'GET',
            'https://pokeapi.co/api/v2/pokedex/3'
        );

        $content = $response->toArray();

        $pkmnNames = [];
        foreach ($content['pokemon_entries'] as $key) {
            $pkmnNames[] = $key['pokemon_species']['name'];
        }
        $content = $pkmnNames;

        return $content;
    }

    /**
     * Cette fonction fait une requête SQL pour récupérer les Pokémon avec les infos que je souhaite afficher dans mon front.
     * 
     * @return array Renvoie Nom / Couleur / Type / Sprite / id pour chaques Pokemon
     */
    public function findAll(): array
    {
        $list = [];
        $query = $this->connection->prepare('SELECT * FROM pokedex');
        $query->execute();
        foreach ($query->fetchAll() as $line) {
            $list[] = new Pokedex($line['nom'], $line['couleur'], $line['type'], $line['sprite'], $line['spriteShiny'], $line['id']);
        }
        return $list;
    }
}