<?php

namespace App\Repository;

use App\Entities\EvolutionChain;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use PDO;

class EvolutionChainRepository
{
    private PDO $connection;

    public function __construct(private HttpClientInterface $client)
    {
        $this->connection = Database::connect();
    }

    /**
     * @param int $id du Pokemon
     * Fetch vers la BDD toutes les infos pour les toutes evolutions d'un Pokemon 
     */
    public function fetchEvolutionChain(int $id)
    {
        //Liste evo existe pas 2G
        $evolutions = array("munchlax","sirfetchd","dudunsparce","cursola","sirfetch'd","mr-rime","farigiraf","perrserker","annihilape","wynaut","clodsire","happiny", "mime-jr", "bonsly", "hisuian sneasel", "hisuian qwilfish", "hisuian voltorb", "hisuian electrode", "hisuian arcanine", "hisuian growlithe", "hisuian typhlosion", "sneasler", "overqwil", "ursaluna", "wyrdeer", "kleavor", "togekiss", "leafeon", "glaceon", "magnezone", "lickilicky", "rhyperior", "tangrowth", "electivire", "magmortar", "yanmega", "ambipom", "mismagius", "honchkrow", "gliscor", "weavile", "mamoswine", "porygon-z", "gallade", "froslass", "dusknoir", "rotom", "leavanny", "sylveon", "slurpuff", "aromatisse", "dragapult", "appletun", "flapple");

        $statement = $this->connection->prepare('INSERT INTO evolution(idEvolutionChain,evolveFromName,evolveFromImg,
        evolveToName,evolveToImage,evolveToLvl,evolveToCondition,evolveToBonheur,evolveToTimeOfDay)
        VALUES (:idEvolutionChain,:evolveFromName,:evolveFromImg,
        :evolveToName,:evolveToImage,:evolveToLvl,:evolveToCondition,:evolveToBonheur,:evolveToTimeOfDay)');

        $response = $this->client->request(
            'GET',
            'https://pokeapi.co/api/v2/evolution-chain/' . $id
        );
        $content = $response->toArray();
        $idEvolutionChain = $content['id'];
        $statement->bindValue('idEvolutionChain', $idEvolutionChain);


        if (isset($content['chain']['species']['name']) && in_array($content['chain']['species']['name'], $evolutions)) {
            // return $content['chain'];
            unset($content['chain']);
        } else {

        //EvolveFrom
        //Route Pokemon
        $pokemonName = $content['chain']['species']['name'];
        $response = $this->client->request(
            'GET',
            'https://pokeapi.co/api/v2/pokemon/' . $pokemonName
        );
        $contentPokemon = $response->toArray();
        $evolveFromImg = $contentPokemon['sprites']['other']['official-artwork']['front_default'];
        $statement->bindValue('evolveFromImg', $evolveFromImg);
        //Route Pokemon Species
        $url = $content['chain']['species']['url'];
        $response = $this->client->request(
            'GET',
            $url
        );
        $contentSpecies = $response->toArray();
        $evolveFromName = $contentSpecies['names']['4']['name'];
        $statement->bindValue('evolveFromName', $evolveFromName);
        //EvolveFrom

        foreach ($content['chain']['evolves_to'] as $key) {
            $statement->bindValue('evolveToCondition', null);
            $statement->bindValue('evolveToBonheur', null);
            $statement->bindValue('evolveToTimeOfDay', null);
            $statement->bindValue('evolveToLvl', null);

            $pokemonName = $key['species']['name'];
            $url = $key['species']['url'];
            $response = $this->client->request(
                'GET',
                $url
            );
            $contentSpecies = $response->toArray();
            //Name Evolution
            $evolveToName = $contentSpecies['names']['4']['name'];
            //if !existe 2G
            if (isset($contentSpecies['name']) && in_array($contentSpecies['name'], $evolutions)) {
                unset($contentSpecies['name']);
            } else {
                $response = $this->client->request(
                    'GET',
                    'https://pokeapi.co/api/v2/pokemon/' . $pokemonName
                );
                $contentPokemon = $response->toArray();
                //Img Evolution
                $evolveToImage = $contentPokemon['sprites']['other']['official-artwork']['front_default'];

                if (isset($key['evolution_details']['0']['relative_physical_stats']) && isset($key['evolution_details']['0']['min_level'])) {
                    $evolveToLvl = $key['evolution_details']['0']['min_level'];
                    $evolveToCondition = $key['evolution_details']['0']['relative_physical_stats'];
                    $statement->bindValue('evolveToLvl', $evolveToLvl);
                    $statement->bindValue('evolveToCondition', $evolveToCondition);
                }
                if (isset($key['evolution_details']['0']['min_level'])) {
                    $evolveToLvl = $key['evolution_details']['0']['min_level'];
                    $statement->bindValue('evolveToLvl', $evolveToLvl);
                }
                if (isset($key['evolution_details']['0']['trigger']['name'])) {
                    $evolveToLvl = $key['evolution_details']['0']['trigger']['name'];
                    $statement->bindValue('evolveToCondition', $evolveToLvl);
                }
                if (isset($key['evolution_details']['0']['item']['name'])) {
                    $url = $key['evolution_details']['0']['item']['url'];
                    $response = $this->client->request(
                        'GET',
                        $url
                    );
                    $contentItem = $response->toArray();
                    $evolveToCondition = $contentItem['names']['3']['name'];
                    $statement->bindValue('evolveToCondition', $evolveToCondition);
                }
                if (isset($key['evolution_details']['0']['held_item']['name'])) {
                    $url = $key['evolution_details']['0']['held_item']['url'];
                    $response = $this->client->request(
                        'GET',
                        $url
                    );
                    $contentItem = $response->toArray();
                    $evolveToCondition = $contentItem['names']['3']['name'];
                    $statement->bindValue('evolveToCondition', $evolveToCondition);
                }
                if (isset($key['evolution_details']['0']['min_happiness']) && $key['evolution_details']['0']['time_of_day'] !== "") {
                    $evolveToBonheur = $key['evolution_details']['0']['min_happiness'];
                    $evolveToTimeOfDay = $key['evolution_details']['0']['time_of_day'];
                    $statement->bindValue('evolveToBonheur', $evolveToBonheur);
                    $statement->bindValue('evolveToTimeOfDay', $evolveToTimeOfDay);
                }
                if (isset($key['evolution_details']['0']['min_happiness'])) {
                    $evolveToBonheur = $key['evolution_details']['0']['min_happiness'];
                    $statement->bindValue('evolveToBonheur', $evolveToBonheur);
                }

                $statement->bindValue('evolveToName', $evolveToName);
                $statement->bindValue('evolveToImage', $evolveToImage);
                $statement->execute();

                //Stade 2
                if (isset($key['evolves_to']['0']['evolution_details'])) {
                    foreach ($key['evolves_to'] as $khey) {
                        //if !existe 2G
                        if (isset($khey['species']['name']) && in_array($khey['species']['name'], $evolutions)) {
                            unset($khey);
                        } else {
                            $statement->bindValue('evolveToCondition', null);
                            $statement->bindValue('evolveToBonheur', null);
                            $statement->bindValue('evolveToTimeOfDay', null);
                            $statement->bindValue('evolveToLvl', null);

                            //EvolveFrom
                            //Route Pokemon
                            $pokemonName = $key['species']['name'];
                            $response = $this->client->request(
                                'GET',
                                'https://pokeapi.co/api/v2/pokemon/' . $pokemonName
                            );
                            $contentPokemon = $response->toArray();
                            $evolveFromImg = $contentPokemon['sprites']['other']['official-artwork']['front_default'];
                            $statement->bindValue('evolveFromImg', $evolveFromImg);
                            //Route Pokemon Species
                            $url = $key['species']['url'];
                            $response = $this->client->request(
                                'GET',
                                $url
                            );
                            $contentSpecies = $response->toArray();
                            $evolveFromName = $contentSpecies['names']['4']['name'];
                            $statement->bindValue('evolveFromName', $evolveFromName);
                            //EvolveFrom

                            if (isset($khey['evolution_details']['0']['min_level'])) {
                                $evolveToLvl = $khey['evolution_details']['0']['min_level'];
                                $statement->bindValue('evolveToLvl', $evolveToLvl);
                            }
                            if (isset($khey['evolution_details']['0']['held_item']['name'])) {
                                $url = $khey['evolution_details']['0']['held_item']['url'];
                                $response = $this->client->request(
                                    'GET',
                                    $url
                                );
                                $contentItem = $response->toArray();
                                $evolveToCondition = $contentItem['names']['3']['name'];
                                $statement->bindValue('evolveToCondition', $evolveToCondition);
                            }
                            if (isset($khey['evolution_details']['0']['trigger']['name'])) {
                                $evolveToLvl = $khey['evolution_details']['0']['trigger']['name'];
                                $statement->bindValue('evolveToCondition', $evolveToLvl);
                            }
                            if (isset($khey['evolution_details']['0']['item']['name'])) {
                                $url = $khey['evolution_details']['0']['item']['url'];
                                $response = $this->client->request(
                                    'GET',
                                    $url
                                );
                                $contentItem = $response->toArray();
                                $evolveToCondition = $contentItem['names']['3']['name'];
                                $statement->bindValue('evolveToCondition', $evolveToCondition);
                            }
                            if (isset($khey['evolution_details']['0']['held_item']['name']) && isset($khey['evolution_details']['0']['trigger']['name'])) {
                                $url = $khey['evolution_details']['0']['held_item']['url'];
                                $response = $this->client->request(
                                    'GET',
                                    $url
                                );
                                $contentItem = $response->toArray();
                                $evolveToCondition = $contentItem['names']['3']['name'];
                                $evolveToLvl = $khey['evolution_details']['0']['trigger']['name'];
                                $evolveToCondition = $evolveToCondition . "|" . $evolveToLvl;

                                $statement->bindValue('evolveToCondition', $evolveToCondition);
                            }

                            $pokemonName = $khey['species']['name'];
                            $url = $khey['species']['url'];
                            $response = $this->client->request(
                                'GET',
                                $url
                            );
                            $contentSpecies = $response->toArray();
                            //Name Evolution
                            $evolveToName = $contentSpecies['names']['4']['name'];

                            $response = $this->client->request(
                                'GET',
                                'https://pokeapi.co/api/v2/pokemon/' . $pokemonName
                            );
                            $contentPokemon = $response->toArray();
                            //Img Evolution
                            $evolveToImage = $contentPokemon['sprites']['other']['official-artwork']['front_default'];

                            $statement->bindValue('evolveToName', $evolveToName);
                            $statement->bindValue('evolveToImage', $evolveToImage);

                            $statement->execute();
                        }
                    }
                }
            }
            }
        }
        return $content;
    }

    /**
     * Fetch toutes les évolutions vers la BDD via un tableau de nom de Pokemon
     */
    public function fetchAllEvolutionChain()
    {
        $httpClient = HttpClient::create();
        $pokemonRepo = new PokemonRepository($httpClient);
        $pokemonIdEvo = $pokemonRepo->getAllPokemonidEvolutionChain();
        $result = [];
        foreach ($pokemonIdEvo as $key) {
            $result[] = $this->fetchEvolutionChain($key["evolutionChainID"]);
        }
        return $result;
    }
}