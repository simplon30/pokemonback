<?php

namespace App\Entities;

class PokemonData
{
    private Pokemon $pokemon;
    private array $evolution;
    
    public function __construct(Pokemon $pokemon, array $evolution)
    {
        $this->pokemon = $pokemon;
        $this->evolution = $evolution;
    }

    public function getPokemon(): Pokemon
    {
        return $this->pokemon;
    }

    public function getEvolution(): array
    {
        return $this->evolution;
    }
}
