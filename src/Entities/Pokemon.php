<?php

namespace App\Entities;

use Symfony\Component\Validator\Constraints as Assert;



class Pokemon
{
	private ?int $id;
	#[Assert\NotBlank]
	private string $nom;
	private string $couleur;
	private string $type;
	private string $sprite;
	private string $spriteShiny;
	private string $description;
	private string $height;
	private string $weight;
	private string $abilities;
	private string $abilitiesDesc;
	private string $encounters;
	private int $Hp;
	private int $Attack;
	private int $Defense;
	private int $specialAttack;
	private int $specialDefense;
	private int $speed;
	private string $eggGroups;
	private int $genderRate;
	private int $captureRate;
	private string $genera;
	private string $shape;
	private ?int $evolutionChainID;

	public function __construct(
		?int $id,
		string $nom,
		string $couleur,
		string $type,
		string $sprite,
		string $spriteShiny,
		string $description,
		string $height,
		string $weight,
		string $abilities,
		string $abilitiesDesc,
		string $encounters,
		int $Hp,
		int $Attack,
		int $Defense,
		int $specialAttack,
		int $specialDefense,
		int $speed,
		string $eggGroups,
		int $genderRate,
		int $captureRate,
		string $genera,
		string $shape,
		int $evolutionChainID
	) {
		$this->id = $id;
		$this->nom = $nom;
		$this->couleur = $couleur;
		$this->type = $type;
		$this->sprite = $sprite;
		$this->spriteShiny = $spriteShiny;
		$this->description = $description;
		$this->height = $height;
		$this->weight = $weight;
		$this->abilities = $abilities;
		$this->abilitiesDesc = $abilitiesDesc;
		$this->encounters = $encounters;
		$this->Hp = $Hp;
		$this->Attack = $Attack;
		$this->Defense = $Defense;
		$this->specialAttack = $specialAttack;
		$this->specialDefense = $specialDefense;
		$this->speed = $speed;
		$this->eggGroups = $eggGroups;
		$this->genderRate = $genderRate;
		$this->captureRate = $captureRate;
		$this->genera = $genera;
		$this->shape = $shape;
		$this->evolutionChainID = $evolutionChainID;
	}



	/**
	 * @return 
	 */
	public function getId(): ?int
	{
		return $this->id;
	}

	/**
	 * @param  $id 
	 * @return self
	 */
	public function setId(?int $id): self
	{
		$this->id = $id;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getNom(): string
	{
		return $this->nom;
	}

	/**
	 * @param string $nom 
	 * @return self
	 */
	public function setNom(string $nom): self
	{
		$this->nom = $nom;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getCouleur(): string
	{
		return $this->couleur;
	}

	/**
	 * @param string $couleur 
	 * @return self
	 */
	public function setCouleur(string $couleur): self
	{
		$this->couleur = $couleur;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getType(): string
	{
		return $this->type;
	}

	/**
	 * @param string $type 
	 * @return self
	 */
	public function setType(string $type): self
	{
		$this->type = $type;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getSprite(): string
	{
		return $this->sprite;
	}

	/**
	 * @param string $sprite 
	 * @return self
	 */
	public function setSprite(string $sprite): self
	{
		$this->sprite = $sprite;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getSpriteShiny(): string
	{
		return $this->spriteShiny;
	}

	/**
	 * @param string $spriteShiny 
	 * @return self
	 */
	public function setSpriteShiny(string $spriteShiny): self
	{
		$this->spriteShiny = $spriteShiny;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getDescription(): string
	{
		return $this->description;
	}

	/**
	 * @param string $description 
	 * @return self
	 */
	public function setDescription(string $description): self
	{
		$this->description = $description;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getHeight(): string
	{
		return $this->height;
	}

	/**
	 * @param string $height 
	 * @return self
	 */
	public function setHeight(string $height): self
	{
		$this->height = $height;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getWeight(): string
	{
		return $this->weight;
	}

	/**
	 * @param string $weight 
	 * @return self
	 */
	public function setWeight(string $weight): self
	{
		$this->weight = $weight;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getAbilities(): string
	{
		return $this->abilities;
	}

	/**
	 * @param string $abilities 
	 * @return self
	 */
	public function setAbilities(string $abilities): self
	{
		$this->abilities = $abilities;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getAbilitiesDesc(): string
	{
		return $this->abilitiesDesc;
	}

	/**
	 * @param string $abilitiesDesc 
	 * @return self
	 */
	public function setAbilitiesDesc(string $abilitiesDesc): self
	{
		$this->abilitiesDesc = $abilitiesDesc;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getEncounters(): string
	{
		return $this->encounters;
	}

	/**
	 * @param string $encounters 
	 * @return self
	 */
	public function setEncounters(string $encounters): self
	{
		$this->encounters = $encounters;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getHp(): int
	{
		return $this->Hp;
	}

	/**
	 * @param int $Hp 
	 * @return self
	 */
	public function setHp(int $Hp): self
	{
		$this->Hp = $Hp;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getAttack(): int
	{
		return $this->Attack;
	}

	/**
	 * @param int $Attack 
	 * @return self
	 */
	public function setAttack(int $Attack): self
	{
		$this->Attack = $Attack;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getDefense(): int
	{
		return $this->Defense;
	}

	/**
	 * @param int $Defense 
	 * @return self
	 */
	public function setDefense(int $Defense): self
	{
		$this->Defense = $Defense;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getSpecialAttack(): int
	{
		return $this->specialAttack;
	}

	/**
	 * @param int $specialAttack 
	 * @return self
	 */
	public function setSpecialAttack(int $specialAttack): self
	{
		$this->specialAttack = $specialAttack;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getSpecialDefense(): int
	{
		return $this->specialDefense;
	}

	/**
	 * @param int $specialDefense 
	 * @return self
	 */
	public function setSpecialDefense(int $specialDefense): self
	{
		$this->specialDefense = $specialDefense;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getSpeed(): int
	{
		return $this->speed;
	}

	/**
	 * @param int $speed 
	 * @return self
	 */
	public function setSpeed(int $speed): self
	{
		$this->speed = $speed;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getEggGroups(): string
	{
		return $this->eggGroups;
	}

	/**
	 * @param string $eggGroups 
	 * @return self
	 */
	public function setEggGroups(string $eggGroups): self
	{
		$this->eggGroups = $eggGroups;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getGenderRate(): int
	{
		return $this->genderRate;
	}

	/**
	 * @param int $genderRate 
	 * @return self
	 */
	public function setGenderRate(int $genderRate): self
	{
		$this->genderRate = $genderRate;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getCaptureRate(): int
	{
		return $this->captureRate;
	}

	/**
	 * @param int $captureRate 
	 * @return self
	 */
	public function setCaptureRate(int $captureRate): self
	{
		$this->captureRate = $captureRate;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getGenera(): string
	{
		return $this->genera;
	}

	/**
	 * @param string $genera 
	 * @return self
	 */
	public function setGenera(string $genera): self
	{
		$this->genera = $genera;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getShape(): string
	{
		return $this->shape;
	}

	/**
	 * @param string $shape 
	 * @return self
	 */
	public function setShape(string $shape): self
	{
		$this->shape = $shape;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getEvolutionChainID(): ?int {
		return $this->evolutionChainID;
	}
	
	/**
	 * @param int $evolutionChainID 
	 * @return self
	 */
	public function setEvolutionChainID(?int $evolutionChainID): self {
		$this->evolutionChainID = $evolutionChainID;
		return $this;
	}
}