<?php

/**
 * Nom evo précédente
 * Image
 * 
 * le lvl d'evolution
 * condition
 * bonheur?
 * 
 * Nom evo suivante
 * Image
 * 
 * id
 * idEvolutionChain
 * 
 * evolveFromName
 * evolveFromImg
 * 
 * evolveToLvl
 * evolveToCondition
 * evolveToBonheur
 * evolveToName
 * evolveToImage
 */

namespace App\Entities;

use Symfony\Component\Validator\Constraints as Assert;


class EvolutionChain
{
    private ?int $id;
    #[Assert\NotBlank]
    private ?int $idEvolutionChain;
    private ?string $evolveFromName;
    private ?string $evolveFromImg;
    private ?string $evolveToName;
    private ?string $evolveToImage;
    private ?int $evolveToLvl;
    private ?string $evolveToCondition;
    private ?int $evolveToBonheur;
    private ?string $evolveToTimeOfDay;

    public function __construct(
        ?int $id,
        ?int $idEvolutionChain,
        ?string $evolveFromName,
        ?string $evolveFromImg,
        ?string $evolveToName,
        ?string $evolveToImage,
        ?int $evolveToLvl,
        ?string $evolveToCondition,
        ?int $evolveToBonheur,
        ?string $evolveToTimeOfDay
    ) {
        $this->id = $id;
        $this->idEvolutionChain = $idEvolutionChain;
        $this->evolveFromName = $evolveFromName;
        $this->evolveFromImg = $evolveFromImg;
        $this->evolveToName = $evolveToName;
        $this->evolveToImage = $evolveToImage;
        $this->evolveToLvl = $evolveToLvl;
        $this->evolveToCondition = $evolveToCondition;
        $this->evolveToBonheur = $evolveToBonheur;
        $this->evolveToTimeOfDay = $evolveToTimeOfDay;
    }

	/**
	 * @return 
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param  $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getIdEvolutionChain(): ?int {
		return $this->idEvolutionChain;
	}
	
	/**
	 * @param  $idEvolutionChain 
	 * @return self
	 */
	public function setIdEvolutionChain(?int $idEvolutionChain): self {
		$this->idEvolutionChain = $idEvolutionChain;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getEvolveFromName(): ?string {
		return $this->evolveFromName;
	}
	
	/**
	 * @param  $evolveFromName 
	 * @return self
	 */
	public function setEvolveFromName(?string $evolveFromName): self {
		$this->evolveFromName = $evolveFromName;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getEvolveFromImg(): ?string {
		return $this->evolveFromImg;
	}
	
	/**
	 * @param  $evolveFromImg 
	 * @return self
	 */
	public function setEvolveFromImg(?string $evolveFromImg): self {
		$this->evolveFromImg = $evolveFromImg;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getEvolveToName(): ?string {
		return $this->evolveToName;
	}
	
	/**
	 * @param  $evolveToName 
	 * @return self
	 */
	public function setEvolveToName(?string $evolveToName): self {
		$this->evolveToName = $evolveToName;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getEvolveToImage(): ?string {
		return $this->evolveToImage;
	}
	
	/**
	 * @param  $evolveToImage 
	 * @return self
	 */
	public function setEvolveToImage(?string $evolveToImage): self {
		$this->evolveToImage = $evolveToImage;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getEvolveToLvl(): ?int {
		return $this->evolveToLvl;
	}
	
	/**
	 * @param  $evolveToLvl 
	 * @return self
	 */
	public function setEvolveToLvl(?int $evolveToLvl): self {
		$this->evolveToLvl = $evolveToLvl;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getEvolveToCondition(): ?string {
		return $this->evolveToCondition;
	}
	
	/**
	 * @param  $evolveToCondition 
	 * @return self
	 */
	public function setEvolveToCondition(?string $evolveToCondition): self {
		$this->evolveToCondition = $evolveToCondition;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getEvolveToBonheur(): ?int {
		return $this->evolveToBonheur;
	}
	
	/**
	 * @param  $evolveToBonheur 
	 * @return self
	 */
	public function setEvolveToBonheur(?int $evolveToBonheur): self {
		$this->evolveToBonheur = $evolveToBonheur;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getEvolveToTimeOfDay(): ?string {
		return $this->evolveToTimeOfDay;
	}
	
	/**
	 * @param  $evolveToTimeOfDay 
	 * @return self
	 */
	public function setEvolveToTimeOfDay(?string $evolveToTimeOfDay): self {
		$this->evolveToTimeOfDay = $evolveToTimeOfDay;
		return $this;
	}
}