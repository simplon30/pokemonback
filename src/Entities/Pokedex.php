<?php

namespace App\Entities;

use Symfony\Component\Validator\Constraints as Assert;



class Pokedex
{
    private ?int $id;
    #[Assert\NotBlank]
    private string $nom;
    #[Assert\NotBlank]
    private string $couleur;
    #[Assert\NotBlank]
    private string $type;
    #[Assert\NotBlank]
    private string $sprite;
    #[Assert\NotBlank]
    private string $spriteShiny;

    public function __construct(string $nom, string $couleur, string $type, string $sprite, string $spriteShiny, ?int $id = null)
    {
        $this->id = $id;
        $this->nom = $nom;
        $this->couleur = $couleur;
        $this->type = $type;
        $this->sprite = $sprite;
        $this->spriteShiny = $spriteShiny;
    }



    /**
     * @return 
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param  $id 
     * @return self
     */
    public function setId(?int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getNom(): string
    {
        return $this->nom;
    }

    /**
     * @param string $nom 
     * @return self
     */
    public function setNom(string $nom): self
    {
        $this->nom = $nom;
        return $this;
    }

    /**
     * @return string
     */
    public function getCouleur(): string
    {
        return $this->couleur;
    }

    /**
     * @param string $couleur 
     * @return self
     */
    public function setCouleur(string $couleur): self
    {
        $this->couleur = $couleur;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type 
     * @return self
     */
    public function setType(string $type): self
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getSprite(): string
    {
        return $this->sprite;
    }

    /**
     * @param string $sprite 
     * @return self
     */
    public function setSprite(string $sprite): self
    {
        $this->sprite = $sprite;
        return $this;
    }

    /**
     * @return string
     */
    public function getSpriteShiny(): string
    {
        return $this->spriteShiny;
    }

    /**
     * @param string $spriteShiny 
     * @return self
     */
    public function setSpriteShiny(string $spriteShiny): self
    {
        $this->spriteShiny = $spriteShiny;
        return $this;
    }
}