<?php

namespace App\Command;

use App\Repository\PokedexRepository;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

// the name of the command is what users type after "php bin/console"
#[AsCommand(name: 'app:fetch-pokedex')]
class FetchPokedexData extends Command
{
    private $repo;

    public function __construct(PokedexRepository $repo)
    {
        $this->repo = $repo;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Fetches Pokedex data');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $data = $this->repo->fetchPokedexData();
        $output->writeln(json_encode($data));

        return Command::SUCCESS;
    }
}