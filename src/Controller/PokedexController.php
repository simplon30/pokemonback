<?php

namespace App\Controller;

use App\Repository\PokedexRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api/pokedex')]
class PokedexController extends AbstractController
{

    private PokedexRepository $repo;

    public function __construct(PokedexRepository $repo)
    {
        $this->repo = $repo;
    }

    #[Route(methods: 'GET')]
    public function all(): JsonResponse
    {
        return $this->json($this->repo->findAll());
    }

    /**
     * Insere tout les Pokemon dans la BDD
     */
    // #[Route('/fetch', methods: 'GET')]
    // public function fetchAll()
    // {
    //     return $this->json($this->repo->fetchPokedexData());
    // }

    /**
     * Permet de Fetch tout les nom des Pokemon en anglais de la 2G
     */
    // #[Route('/fetchName', methods: 'GET')]
    // public function fetchNames()
    // {
    //     return $this->json($this->repo->fetchPokemonName());
    // }

}