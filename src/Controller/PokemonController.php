<?php

namespace App\Controller;

use App\Repository\PokemonRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('/api/pokemon')]
class PokemonController extends AbstractController
{
    private PokemonRepository $repo;

    public function __construct(PokemonRepository $repo)
    {
        $this->repo = $repo;
    }

    /**
     * @return JsonResponse Tout les Pokemon
     */
    #[Route(methods: 'GET')]
    public function all(): JsonResponse
    {
        return $this->json($this->repo->findAll());
    }

    /**
     * @return mixed One Pokemon by ID
     * @param int $id du Pokemon à GET
     */
    #[Route('/id/{id}', methods: 'GET')]
    public function byId(int $id): JsonResponse
    {
        return $this->json($this->repo->findById($id));
    }

    /**
     * @return mixed One Pokemon by Name
     * @param string $name du Pokemon
     */
    #[Route('/{nom}', methods: 'GET')]
    public function byName(string $nom): JsonResponse
    {
        return $this->json($this->repo->findByName($nom));
    }

    /**
     * Méthode d'exemple si jamais on devrais mettre à jour un Pokemon
     */
    // #[Route('/{id}', methods: 'PUT')]
    public function put(int $id, Request $request, SerializerInterface $serializer)
    {
        $pokemon = $this->repo->findById($id);
        if (!$pokemon) {
            throw new NotFoundHttpException();
        }
        $toUpdate = $serializer->deserialize($request->getContent(), Pokemon::class, 'json');
        $toUpdate->setId($id);
        $this->repo->update($toUpdate);
        return $this->json($toUpdate);
    }

    /**
     * Méthode d'exemple si jamais on devrais supprimer un Pokemon
     */
    // #[Route('/{id}', methods: 'DELETE')]
    // public function remove(int $id)
    // {
    //     $pokemon = $this->repo->findById($id);
    //     if (!$pokemon) {
    //         throw new NotFoundHttpException();
    //     }
    //     $this->repo->delete($pokemon);
    //     return $this->json(null, Response::HTTP_NO_CONTENT);
    // }

    /**
     * Permet de fetch tout les Pokemon pour ensuite les push dans la BDD Pokemon
     */
    // #[Route('/fetch/Pokedex', methods: 'GET')]
    // public function fetchAllPokemonIntoBDD()
    // {
    //     return $this->json($this->repo->fetchAllPokemonbyName());
    // }

    /**
     * @param string $name nom en anglais du Pokemon
     * Cette route permet de Fetch un Pokemon
     * Surtout utilisé pour des test, à ne pas utiliser. 
     */
    // #[Route('/fetch/{name}', methods: 'GET')]
    // public function fetchOneByName(string $name)
    // {
    //     return $this->json($this->repo->fetchPokemonbyName($name));
    // }

}