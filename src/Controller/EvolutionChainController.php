<?php

namespace App\Controller;

use App\Repository\EvolutionChainRepository;
use App\Repository\PokemonRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api/evolutionChain')]
class EvolutionChainController extends AbstractController
{
    private EvolutionChainRepository $repo;
    public function __construct(EvolutionChainRepository $repo)
    {
        $this->repo = $repo;
    }

    /**
     * @param int $id du Pokemon
     * Fetch evolutionChain pour un Pokemon vers la BDD
     */
    // #[Route('/{id}', methods: 'GET')]
    // public function one(int $id): JsonResponse
    // {
    //     return $this->json($this->repo->fetchEvolutionChain($id));
    // }

    /**
     * Fetch toutes les Evo vers la BDD
     */
    // #[Route(methods: 'GET')]
    // public function fetchPokemonEvolution()
    // {
    //     return $this->json($this->repo->fetchAllEvolutionChain());
    // }
}