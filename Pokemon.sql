-- Active: 1673947616242@@127.0.0.1@3306

DROP TABLE IF EXISTS pokedex;

DROP TABLE IF EXISTS pokemon;

DROP TABLE IF EXISTS evolution;

CREATE TABLE
    pokedex(
        id INT PRIMARY KEY AUTO_INCREMENT,
        nom VARCHAR(25),
        couleur VARCHAR(25),
        type VARCHAR(25),
        sprite TEXT,
        spriteShiny TEXT
    );

CREATE TABLE
    pokemon (
        id INT PRIMARY KEY AUTO_INCREMENT,
        nom VARCHAR(25),
        couleur VARCHAR(25),
        type VARCHAR(25),
        sprite TEXT,
        spriteShiny TEXT,
        description TEXT,
        height VARCHAR(10),
        weight VARCHAR(10),
        abilities VARCHAR(255),
        abilitiesDesc TEXT,
        encounters TEXT,
        -- STATS
        Hp INT,
        Attack INT,
        Defense INT,
        specialAttack INT,
        specialDefense INT,
        speed INT,
        eggGroups VARCHAR (255),
        genderRate INT,
        captureRate INT,
        genera VARCHAR(255),
        shape VARCHAR(255),
        evolutionChainID INT
         -- MOVES ??? autre table ??
        -- name VARCHAR(255),
        -- level_learned_at INT,
        -- https://pokeapi.co/api/v2/move/28/
        -- accuracy INT,
        -- flavor_text VARCHAR(255),
        -- pp INT,
        -- power INT,
        -- damage_class INT,  --( physique / spécial)
    );

CREATE TABLE
    evolution(
        id INT PRIMARY KEY AUTO_INCREMENT,
        idEvolutionChain INT,
        evolveFromName VARCHAR(255),
        evolveFromImg TEXT,
        evolveToName VARCHAR(255),
        evolveToImage TEXT,
        evolveToLvl INT,
        evolveToCondition VARCHAR(255),
        evolveToBonheur INT,
        evolveToTimeOfDay VARCHAR(10)
    );