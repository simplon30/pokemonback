<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PokemonTest extends WebTestCase
{
    public function testGetAll()
    {
        $client = static::createClient();

        $client->request('GET', '/api/pokemon');

        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(200);

        $body = json_decode($client->getResponse()->getContent(), true);

        $this->assertCount(251, $body);

        $pokemon = $body[0];
        $this->assertArrayHasKey('id', $pokemon);
        $this->assertArrayHasKey('nom', $pokemon);
        $this->assertArrayHasKey('couleur', $pokemon);
        $this->assertArrayHasKey('type', $pokemon);
        $this->assertArrayHasKey('sprite', $pokemon);
        $this->assertArrayHasKey('spriteShiny', $pokemon);
        $this->assertArrayHasKey('description', $pokemon);
        $this->assertArrayHasKey('height', $pokemon);
        $this->assertArrayHasKey('weight', $pokemon);
        $this->assertArrayHasKey('abilities', $pokemon);
        $this->assertArrayHasKey('abilitiesDesc', $pokemon);
        $this->assertArrayHasKey('encounters', $pokemon);
        $this->assertArrayHasKey('Hp', $pokemon);
        $this->assertArrayHasKey('Attack', $pokemon);
        $this->assertArrayHasKey('Defense', $pokemon);
        $this->assertArrayHasKey('specialAttack', $pokemon);
        $this->assertArrayHasKey('specialDefense', $pokemon);
        $this->assertArrayHasKey('speed', $pokemon);
        $this->assertArrayHasKey('eggGroups', $pokemon);
        $this->assertArrayHasKey('genderRate', $pokemon);
        $this->assertArrayHasKey('captureRate', $pokemon);
        $this->assertArrayHasKey('genera', $pokemon);
        $this->assertArrayHasKey('shape', $pokemon);
        $this->assertArrayHasKey('evolutionChainID', $pokemon);
    }
    
}