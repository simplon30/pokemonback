<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PokedexTest extends WebTestCase
{
    public function testGetAll() {
        
        $client = static::createClient();
        
        $client->request('GET', '/api/pokedex');

        $this->assertResponseIsSuccessful();

        $body = json_decode($client->getResponse()->getContent(), true);

        $this->assertCount(251, $body);
        $item = $body[0];
        $this->assertArrayHasKey('id', $item);
        $this->assertIsInt($item['id']);
        $this->assertArrayHasKey('nom', $item);
        $this->assertArrayHasKey('couleur', $item);
        $this->assertArrayHasKey('type', $item);
        $this->assertArrayHasKey('sprite', $item);
        $this->assertArrayHasKey('spriteShiny', $item);
    }    
}