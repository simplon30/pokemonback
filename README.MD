# Projet Pokédex - Gestion des données via API

## Introduction

Ce projet consiste à créer un Pokédex en utilisant une API pour récupérer les données Pokémon en anglais, puis les traduire en français pour les afficher côté front-end. L'objectif principal est d'optimiser les performances pour obtenir des réponses rapides.

## Méthode

1. Utilisation d'une API en anglais pour obtenir les données Pokémon.
2. Tri des données en utilisant des requêtes supplémentaires.
3. Traduction des informations en français pour améliorer l'expérience utilisateur.
4. Optimisation des performances pour des temps de réponse rapides.
5. Chargement des données dans une base de données pour le Pokédex et les Pokémon.
6. Les entités Pokémon contiennent des informations telles que sprites, chroma et idEvo.
7. Utilisation de l'ID d'évolution (idEvo) pour lier les Pokémon à leur(s) évolution(s) via l'ID de la chaîne d'évolution (evolutionChain.id).
8. Mise en place des routes pour les fonctionnalités suivantes :
   - "get all" pour le Pokédex.
   - "get by id" pour chaque Pokémon.

## Installation

1. Clonez ce dépôt sur votre machine locale.
2. Accédez au répertoire de votre projet Symfony.
3. Installez les dépendances en utilisant Composer :
    ```
    composer install
    ```

Assurez-vous d'avoir Composer installé sur votre machine avant d'exécuter cette commande.

## Lancement du projet

Pour lancer le projet, vous pouvez exécuter la commande suivante :
   ```
   make serve
   ```
Cette commande démarrera le serveur Symfony et lancera votre projet.
